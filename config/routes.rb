Rails.application.routes.draw do
  get 'admin/index'

  get 'admin/category'

  get 'admin/product'

  get 'admin/order'

  get 'admin/client'

  get 'admin/sklep'

  get 'bill/show'

  get 'bill/platnosc'

  get 'home/index'

  get 'home/contact'

  get 'home/about'

  get 'home/help'

  get 'category/show'

  root to: "home#index"

  resources :category #-> category/:id

  mount Shoppe::Engine => "/shoppe"
  get "product/:permalink", to: "products#show", as: "product"
  post "product/:permalink", to: "products#buy", as: "buy"
  post "product/:permalink", to: "products#buy"
  #root to: "products#index"
  get "basket", to: "orders#show"
  delete "basket", to: "orders#destroy"

  match "checkout", to: "orders#checkout", as: "checkout", via: [:get, :patch]
  match "checkout/pay", to: "orders#payment", as: "checkout_payment", via: [:get, :post]
  match "checkout/confirm", to: "orders#confirmation", as: "checkout_confirmation", via: [:get, :post]
end
