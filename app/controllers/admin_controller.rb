class AdminController < ApplicationController
    

  def index
    @cat = Shoppe::ProductCategory.all
    @order = Shoppe::Order.all
    @product = Shoppe::Product.all
    @client = Shoppe::Customer.all
  end

  def category
  end

  def product
  end

  def order
  end

  def client
  end

  def sklep
    @sklep = SklepDane.find_by_id(1)
  end
end
