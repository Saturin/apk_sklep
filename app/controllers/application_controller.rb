class ApplicationController < ActionController::Base

  before_filter :sklep_nazwa, :kategorie, :dane_sklep
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  #wyszukuje nazwe sklepu z ustawień
  def sklep_nazwa
    @nazwa = Shoppe::Setting.find_by_id(1).value
  end
  #
  def kategorie
    @kategorie = Shoppe::ProductCategory.all
    @kategorie.group_by(&:name)
  end

  def kategoria
    @kategoria = Shoppe::ProductCategory.root.find_by_permalink(params[:permalink])
  end

  def dane_sklep
    @sklep = SklepDane.find_by_id(1)
  end

  private

  # funkcja obsługująca koszyk zamówień z biblioteki shoppe
  def current_order
    @current_order ||= begin
      if has_order?
        @current_order
      else
        order = Shoppe::Order.create(:ip_address => request.ip)
        session[:order_id] = order.id
        order
      end
    end
  end
  #funkcja sprawdzająca czy zostało dokonane zamówienie z biblioteki shoppe
  def has_order?
    !!(
      session[:order_id] &&
      @current_order = Shoppe::Order.includes(:order_items => :ordered_item).find_by_id(session[:order_id])
    )
  end

  helper_method :current_order, :has_order?

end
