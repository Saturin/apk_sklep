class HomeController < ApplicationController
  def index
  end

  def contact
  end

  def about
  end

  def help
  end
end
