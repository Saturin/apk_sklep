class CreateSklepDanes < ActiveRecord::Migration
  def change
    create_table :sklep_danes do |t|
      t.string :nazwa
      t.string :adres
      t.integer :konto

      t.timestamps
    end
  end
end
